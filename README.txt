********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Access Perm Group Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Enables modules to add to the access permissions of other modules. 

Useful for simplifying the Access Control page.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.
  
  
********************************************************************
USAGE

hook_access_perm_group_add($client_label, $services)

Example:

/**
 * Implementation of hook_access_perm_group_add()
 */
function foo_access_perm_group_add($client_label, $services) {
	
	if (!in_array('foo', $services)) return;
	
	$foo_types_all = foo_get_types();

	foreach ($foo_types_all as $type) {
		$perm['administer ' . $client_label] = 'administer ' . $client_label;
		$perm['subscribe to ' . $client_label] = 'subscribe to ' . $client_label;
	}
	
	return $perm; 
}



********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/access_perm_group
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT


